package com.example.backend.service;

import com.example.backend.BackendApplication;
import com.example.backend.rest.dto.CarDto;
import com.example.backend.rest.dto.UserDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest(classes = BackendApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServicesTest {
    @Autowired
    UserServices userServices;

    @Test
    public void testGetAllUsers() throws ExecutionException, InterruptedException {
        Assert.assertEquals(userServices.getAllUsers().get().size(), 5);
    }

    @Test
    public void testGetUsersByNameSort() throws ExecutionException, InterruptedException {
        UserDto userDto1 = UserDto.builder().id(1L).name("Teet Järveküla").cars(userServices.getUserCars(1L).get()).build();
        UserDto userDto2 = UserDto.builder().id(5L).name("Teet Kruus").cars(userServices.getUserCars(5L).get()).build();
        List<UserDto> sortedUserDtoList = new ArrayList<>();
        sortedUserDtoList.add(userDto1);
        sortedUserDtoList.add(userDto2);
        for(int i=0; i<sortedUserDtoList.size();i++){
            Assert.assertEquals(sortedUserDtoList.get(i).getId(),userServices.getUsersByNameSort("Teet", "name:asc").get().get(i).getId());
        }
//        Assert.assertEquals(sortedUserDtoList, userServices.getUsersByNameSort("Teet", "name:asc").get());
    }

    @Test
    public void testGetUser() throws ExecutionException, InterruptedException {
        UserDto userDto1 = UserDto.builder().id(2L).name("Pille Purk").cars(userServices.getUserCars(2L).get()).build();
        Assert.assertEquals(userServices.getUser(2L).get(), userDto1);
    }

    @Test
    public void testGetUserCars() throws ExecutionException, InterruptedException {
        List<CarDto> userCarsDto = new ArrayList<>();
        CarDto carDto1 = CarDto.builder().id(1L).make("Lada").model("2101").numberplate("123ASD").build();
        CarDto carDto2 = CarDto.builder().id(2L).make("Kia").model("Sorento").numberplate("534TTT").build();
        userCarsDto.add(carDto1);
        userCarsDto.add(carDto2);
        Assert.assertEquals(userServices.getUserCars(1L).get(), userCarsDto);
    }

}
