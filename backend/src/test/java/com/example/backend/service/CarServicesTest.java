package com.example.backend.service;

import com.example.backend.BackendApplication;
import com.example.backend.rest.dto.CarDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest(classes = BackendApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class CarServicesTest {

    @Autowired
    private CarServices carServices;


    @Test
    public void testGetAllCars() throws ExecutionException, InterruptedException {
        List<CarDto> expectedCarDtoList = carServices.getAllCars().get();
        Assert.assertEquals(expectedCarDtoList.size(), 9);
    }

    @Test
    public void testGetCar() throws ExecutionException, InterruptedException {
        CarDto actualCarDto = carServices.getCar(2L).get();
        CarDto expectedCarDto = CarDto.builder().id(2L).model("Sorento").make("Kia").numberplate("534TTT").build();
        Assert.assertEquals(expectedCarDto, actualCarDto);
    }

    @Test
    public void testSearch() throws ExecutionException, InterruptedException {
        List<CarDto> expectedCarDtoList = new ArrayList<>();
        CarDto carDto1,carDto2;
        carDto1= CarDto.builder().id(2L).model("Sorento").make("Kia").numberplate("534TTT").build();
        carDto2 = CarDto.builder().id(4L).model("Sorento").make("Kia").numberplate("555TFF").build();
        expectedCarDtoList.add(carDto1);
        expectedCarDtoList.add(carDto2);
        Assert.assertEquals(expectedCarDtoList, carServices.searchCarsByParameters("Kia", null, null,null).get());
        expectedCarDtoList.clear();
        carDto1 = CarDto.builder().id(6L).model("A6").make("Audi").numberplate("997HHH").build();
        carDto2 = CarDto.builder().id(8L).model("A6").make("Audi").numberplate("876OUI").build();
        expectedCarDtoList.add(carDto1);
        expectedCarDtoList.add(carDto2);
        Assert.assertEquals(expectedCarDtoList, carServices.searchCarsByParameters(null, "A6", null,null).get());
        expectedCarDtoList.clear();
        carDto1 = CarDto.builder().id(6L).model("A6").make("Audi").numberplate("997HHH").build();
        carDto2 = CarDto.builder().id(8L).model("A6").make("Audi").numberplate("876OUI").build();
        expectedCarDtoList.add(carDto2);
        expectedCarDtoList.add(carDto1);
        Assert.assertEquals(expectedCarDtoList, carServices.searchCarsByParameters(null, "A6", null,"id:desc").get());
        expectedCarDtoList.clear();
        carDto2 = CarDto.builder().id(9L).model("740").make("BMW").numberplate("112YUI").build();
        expectedCarDtoList.add(carDto2);
        Assert.assertEquals(expectedCarDtoList, carServices.searchCarsByParameters(null, null, "112YUI" ,null).get());
    }
}
