insert into cars (id, make, model, numberplate) values (1, 'Lada', '2101', '123ASD');
insert into cars (id, make, model, numberplate) values (2, 'Kia', 'Sorento', '534TTT');
insert into cars (id, make, model, numberplate) values (3, 'Skoda', 'Octavia', '999GLF');
insert into cars (id, make, model, numberplate) values (4, 'Kia', 'Sorento', '555TFF');
insert into cars (id, make, model, numberplate) values (5, 'Lada', '2101', '445KKK');
insert into cars (id, make, model, numberplate) values (6, 'Audi', 'A6', '997HHH');
insert into cars (id, make, model, numberplate) values (7, 'BMW', '760', '444RRR');
insert into cars (id, make, model, numberplate) values (8, 'Audi', 'A6', '876OUI');
insert into cars (id, make, model, numberplate) values (9, 'BMW', '740', '112YUI');

insert into users (id, name) values (1, 'Teet Järveküla');
insert into users (id, name) values (2, 'Pille Purk');
insert into users (id, name) values (3, 'Mati Kaal');
insert into users (id, name) values (4, 'Külli Kukk');
insert into users (id, name) values (5, 'Teet Kruus');

insert into users_cars (user_id, cars_id) values (1, 1);
insert into users_cars (user_id, cars_id) values (1, 2);
insert into users_cars (user_id, cars_id) values (2, 3);
insert into users_cars (user_id, cars_id) values (2, 4);
insert into users_cars (user_id, cars_id) values (3, 5);
insert into users_cars (user_id, cars_id) values (3, 6);
insert into users_cars (user_id, cars_id) values (4, 7);
insert into users_cars (user_id, cars_id) values (4, 8);
insert into users_cars (user_id, cars_id) values (5, 9);



