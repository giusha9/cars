package com.example.backend.service;

import com.example.backend.domain.model.Car;
import com.example.backend.domain.model.User;
import com.example.backend.domain.repositories.CustomUserRepository;
import com.example.backend.domain.repositories.UserRepository;
import com.example.backend.rest.dto.CarDto;
import com.example.backend.rest.dto.UserDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
public class UserServices {
    private UserRepository userRepository;

    private CustomUserRepository customUserRepository;

    public UserServices(UserRepository userRepository, CustomUserRepository customUserRepository) {
        this.userRepository = userRepository;
        this.customUserRepository = customUserRepository;
    }

    public CompletableFuture<List<UserDto>> getAllUsers() {
        List<User> users = (List<User>) userRepository.findAll();
        List<UserDto> userDtoList = new ArrayList<>();
        for (User user : users) {

            List<CarDto> carDtoList = CarServices.carDtoConverter(user.getCars());
            userDtoList.add(UserDto.builder().id(user.getId())
                    .name(user.getName())
                    .cars(carDtoList).build());
        }
        return CompletableFuture.completedFuture(userDtoList);
    }

    public CompletableFuture<List<UserDto>> getUsersByNameSort(String name, String sort) {
        String order = null;
        if (sort != null) {
            order = sort.split(":")[1];
        }
        List<User> userList = customUserRepository.searchUsersByName(name, order);
        List<UserDto> userDtoList = new ArrayList<>();
        for (User user : userList) {

            List<CarDto> carDtoList = CarServices.carDtoConverter(user.getCars());
            userDtoList.add(UserDto.builder().id(user.getId())
                    .name(user.getName())
                    .cars(carDtoList).build());
        }
        return CompletableFuture.completedFuture(userDtoList);
    }

    public CompletableFuture<UserDto> getUser(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        User user;
        if (userOptional.isPresent()) {
            user = userOptional.get();
            List<CarDto> carDtoList = CarServices.carDtoConverter(user.getCars());
            UserDto userDto = UserDto.builder().id(user.getId())
                    .name(user.getName())
                    .cars(carDtoList).build();
            return CompletableFuture.completedFuture(userDto);
        } else {
            return null;
        }
    }

    public CompletableFuture<List<CarDto>> getUserCars(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        User user;
        if (userOptional.isPresent()) {
            user = userOptional.get();
            return CompletableFuture.completedFuture(CarServices.carDtoConverter(user.getCars()));
        } else {
            return null;
        }
    }

}
