package com.example.backend.service;

import com.example.backend.domain.model.Car;
import com.example.backend.domain.repositories.CarRepository;
import com.example.backend.domain.repositories.CustomCarRepository;
import com.example.backend.rest.dto.CarDto;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
public class CarServices {
    CarRepository carRepository;
    CustomCarRepository customCarRepository;

    public CarServices(CarRepository carRepository, CustomCarRepository customCarRepository) {
        this.carRepository = carRepository;
        this.customCarRepository = customCarRepository;
    }
    @Async
    public CompletableFuture<List<CarDto>> getAllCars() {
        List<Car> cars = (List<Car>) carRepository.findAll();
        List<CarDto> carDtos = new ArrayList<>();
        for (Car car : cars) {
            carDtos.add(CarDto.builder().id(car.getId())
                    .make(car.getMake())
                    .model(car.getModel())
                    .numberplate(car.getNumberplate()).build());
        }
        return CompletableFuture.completedFuture(carDtos);
    }
    @Async
    public CompletableFuture<CarDto> getCar(Long id) {
        Optional<Car> carOptional = carRepository.findById(id);
        Car car;
        if (carOptional.isPresent()) {
            car = carOptional.get();
            CarDto carDto = CarDto.builder().id(car.getId())
                    .make(car.getMake())
                    .model(car.getModel())
                    .numberplate(car.getNumberplate())
                    .build();
            return CompletableFuture.completedFuture(carDto);
        } else {
            return null;
        }
    }

    public CompletableFuture<List<CarDto>> searchCarsByParameters(String make, String model, String numberplate, String sort) {
        String order = null;
        String orderBy = null;
        if(sort!=null){
            order = sort.split(":")[0];
            orderBy = sort.split(":")[1];
        }
        List<Car> cars = customCarRepository.searchCarsByParameters(make, model, numberplate, orderBy, order);

        return CompletableFuture.completedFuture(carDtoConverter(cars));
    }

    static List<CarDto> carDtoConverter(List<Car> cars) {
        List<CarDto> carDtos = new ArrayList<>();
        for (Car car : cars) {
            carDtos.add(CarDto.builder().id(car.getId())
                    .model(car.getModel())
                    .make(car.getMake())
                    .numberplate(car.getNumberplate()).build());
        }
        return carDtos;
    }

}
