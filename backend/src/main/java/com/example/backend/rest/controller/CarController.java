package com.example.backend.rest.controller;

import com.example.backend.Constants;
import com.example.backend.rest.dto.CarDto;
import com.example.backend.service.CarServices;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(Constants.API_URL + "/cars")
public class CarController extends BaseController {
    private CarServices carServices;

    public CarController(CarServices carServices) {
        this.carServices = carServices;
    }

    @GetMapping
    public ResponseEntity<List<CarDto>> getCars(@RequestParam(name = "find", required = false) String make,
                                                @RequestParam(name = "model", required = false) String model,
                                                @RequestParam(name = "numberplate", required = false) String numberplate,
                                                @RequestParam(name = "sort", required = false) String sort) throws Exception {
        List<CarDto> carDtos;
        if (make == null && model == null && numberplate == null && sort == null) {
            carDtos = carServices.getAllCars().get();
        } else {
            carDtos = carServices.searchCarsByParameters(make, model, numberplate, sort).get();
        }
        if (carDtos.size() == 0) {
            throw new Exception("cars not found");
        } else {
            return new ResponseEntity<>(carDtos, HttpStatus.OK);
        }
    }

    @GetMapping("/{Id}")
    public ResponseEntity<CarDto> getCar(@PathVariable Long Id) throws Exception {
        if (carServices.getCar(Id).get() != null)
            return new ResponseEntity<>(carServices.getCar(Id).get(), HttpStatus.OK);
        else {
            throw new Exception("car not found");
        }
    }

}
