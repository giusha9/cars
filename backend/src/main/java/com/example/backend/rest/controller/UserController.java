package com.example.backend.rest.controller;

import com.example.backend.Constants;
import com.example.backend.domain.model.User;
import com.example.backend.rest.dto.CarDto;
import com.example.backend.rest.dto.UserDto;
import com.example.backend.service.UserServices;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Constants.API_URL + "/users")
public class UserController extends BaseController {
    private UserServices userServices;

    public UserController(UserServices userServices) {
        this.userServices = userServices;
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> getAllUsers(@RequestParam(name = "find", required = false) String name,
                                                     @RequestParam(name = "sort", required = false) String sort) throws Exception {
        List<UserDto> usersDtoList;
        if (name == null && sort == null) {
            usersDtoList = userServices.getAllUsers().get();
        } else {
            usersDtoList = userServices.getUsersByNameSort(name, sort).get();
        }
        if (usersDtoList == null) {
            throw new Exception("users wasnt found");
        } else {
            return new ResponseEntity<>(usersDtoList, HttpStatus.OK);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable Long id) throws Exception {
        UserDto userDto = userServices.getUser(id).get();
        if (userDto != null) {
            return new ResponseEntity<>(userDto, HttpStatus.OK);
        } else {
            throw new Exception("User not found");
        }
    }

    @GetMapping("/{id}/cars")
    public ResponseEntity<List<CarDto>> getUserCars(@PathVariable Long id) throws Exception {
        List<CarDto> carDtoList = userServices.getUserCars(id).get();
        if (carDtoList != null) {
            return new ResponseEntity<>(carDtoList, HttpStatus.OK);
        }
        else{
            throw new Exception("Cars was not found");
        }
    }

}
