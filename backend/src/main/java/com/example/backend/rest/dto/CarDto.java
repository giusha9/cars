package com.example.backend.rest.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CarDto {
    Long id;
    private String make;
    private String model;
    private String numberplate;
}
