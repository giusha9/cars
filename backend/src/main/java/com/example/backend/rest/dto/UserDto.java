package com.example.backend.rest.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserDto {
    Long id;
    private String name;
    private List<CarDto> cars;
}
