package com.example.backend.rest.controller;

import com.example.backend.ErrorCode;
import com.example.backend.rest.dto.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class GlobalControllerExceptionHandler extends BaseController {

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Throwable t) {
        log.error("Catched exception: ", t);
        return new ResponseEntity<>(new ErrorResponse(ErrorCode.RESOURCE_NOT_FOUND, t.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
