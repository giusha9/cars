package com.example.backend.domain.repositories;

import com.example.backend.domain.model.Car;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class CarRepositoryImpl implements CustomCarRepository {
    @Autowired
    EntityManager em;

    @Override
    public List<Car> searchCarsByParameters(String make, String model, String numberplate, String order, String orderBy) {
        StringBuilder query = new StringBuilder("select car from Car car where (?1 is null or lower(car.make) LIKE lower(?1) || '%') and "
                + "(?2 is null or lower(car.model) LIKE lower(?2) || '%') and (?3 is null or lower(car.numberplate) LIKE lower(?3) || '%')");
        if (orderBy != null) {
            query.append(" ORDER BY car.").append(orderBy);
        }
        if (order != null) {
            query.append(" ").append(order);
        }
        return em.createQuery(query.toString(), Car.class)
                .setParameter(1, make)
                .setParameter(2, model)
                .setParameter(3, numberplate)
                .getResultList();
    }

}
