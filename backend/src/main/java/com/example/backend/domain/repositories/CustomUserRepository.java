package com.example.backend.domain.repositories;

import com.example.backend.domain.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomUserRepository {
    List<User> searchUsersByName(String name, String order);

}
