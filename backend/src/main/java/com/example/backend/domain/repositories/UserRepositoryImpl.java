package com.example.backend.domain.repositories;

import com.example.backend.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepositoryImpl implements CustomUserRepository {
    @Autowired
    EntityManager em;

    @Override
    public List<User> searchUsersByName(String name, String order) {
        StringBuilder query = new StringBuilder("select user from User user where lower(user.name) LIKE lower(?1) || '%' ");
        if(order!=null)
            query.append("Order by user.name ").append(order);
        return em.createQuery(query.toString(), User.class)
                .setParameter(1, name)
                .getResultList();
    }
}
