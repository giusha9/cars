package com.example.backend.domain.repositories;

import com.example.backend.domain.model.Car;
import org.springframework.stereotype.Repository;


@Repository
public interface CarRepository extends ReadOnlyRepository<Car, Long> {


}
