package com.example.backend.domain.repositories;

import com.example.backend.domain.model.Car;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CustomCarRepository {
    List<Car> searchCarsByParameters(String make, String model, String numberplate, String orderBy, String order);
}
