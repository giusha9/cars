package com.example.backend.domain.repositories;

import com.example.backend.domain.model.Car;
import com.example.backend.domain.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends ReadOnlyRepository<User,Long>{
    List<User> findUserByNameContainingIgnoreCase(String name);
}
