import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/views/HelloWorld'
import Car from '@/views/Car'
import User from '@/views/User'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld,
      props: {msg:"Welcome to the Vue.js app"}
    },
    {
      path: '/cars',
      name: 'Car',
      component: Car
    },
    {
      path: '/users',
      name: 'User',
      component: User
    }
  ]
})
